---
title: "Scuffed Cuff"
subtitle: "Improvised Heists for Uune"
author: "Benrob0329"
---

::: title-page :::

:::: title ::::
# Scuffed Cuff
Improvised Heists for Uune
::::

:::: copyright ::::
To the extent possible under law, Benrob0329 has waived all copyright and related or neighboring rights to this work.

[http://creativecommons.org/publicdomain/zero/1.0/](http://creativecommons.org/publicdomain/zero/1.0/)
::::
:::

## Introduction

Welcome to Earth. The year is 2022, the world still reeling from the global pandemic, many people have opted to work from home on a more permanent basis. Many are finding new hobbies, even careers doing things they couldn't have dreamed of making a living off of but a few years prior. While we may never know the full extent of the changes the pandemic has brought, it's given everyone a unique perspective that will be impossible to repeat without having lived through it.

Now forget all of that because the world ended in 2012.

Welcome to Baxfield, New York. A smaller city somewhere north of the Big Apple. When the world ended in 2012, most major governments pulled funding from the lesser populated areas and outlying territories, resulting in civil unrest. In the ensuing power-gap, a lot of criminal organizations seized the opportunity to take portions of the city for themselves, sometimes even whole districts.

Over the years, these factions have fought with each other and attempted to stamp the others out, resulting in continued unrest for those uninvolved. The general turmoil of the area caused many companies to either go out of business or close up shop and move elsewhere. What little remained was either under the control of a mafia group, or in the sparse neutral zones between.
"Now that funding is trickling back in, a local police force has been slowly pushing back the influence of the various criminal organizations. For most, that would be a good thing; for you however, that might spell disaster, _or_..opportunity.

In this city, you are a criminal, perhaps a vigilante, who's bread and butter relies on the chaos that had brought this city to it's knees for so long. Now, with the threat of order rising once again, you'll have to ride the line of reputable and dangerous if you want to survive.

## World Info

### City Districts

#### The Docks
Ruffles of water ebb off the shoreline, while a pinch of salt mixes with the air. Weathered boards creak to the step of each dock hand, as crates of supplies and nets of fish are carried from their floating canisters.

These parts are home to some of the hardest working men and women of this city, but they are also home to some nasty smuggling operations. While no one organization lays claim to it, there are many portions which are fenced off as private land for the likes of marina companies and mafia alike.

#### The Dorms
A crumple of paper rustles softly in the wind, only to be overpowered by gunshots in the distance. The sun stretches over the tips of buildings towering down the street, glistening in the windows not yet illuminated over the evening light.

People live here, people of many creeds and kinds. Some are just passing though, while others have been here since this city was a much smaller place. Condos, hotels, gated communities from a bygone era all find their place here. For many, it's the only home they can afford in these trying times, though even then it's hard to imagine that anyone else could be doing much better.

#### The Suburbs
Further outside the inner city are the suburbs. Here, some of the better off denizens of the city hold down what little is left to claim as their own.

#### The Sticks
Further out still are the out-of-townies: farmers, animal tenders, etc. Not a lot happens out here, and it's residents would like to keep it that way.

#### The Shops
On the north end of town are the shops, a combination of old storefronts and some industrial buildings. Much of the mafia holds up here, usually in old warehouses and parking garages.

#### Downtown
In the middle of the city is the old downtown area. This is where many of the historical buildings, small stores, and the governmental building reside.

### Major Factions

The Mafia
During the collapse, a local power gap left much of the city in disarray. In the chaos, one man would assemble his forces and form a local militia: Rufo Medici. Over the years, his mafia would bail out businesses with loans to get other mobs off their backs, offer protection from other organizations, and then collect "taxes" from people under his thumb and in his debt.

At one time, if you asked anyone they'd tell you that this was Medici's city, but with the local police force reforming he's been pushed back. His mob is a shell of it's former self, but he's still holding out that he can maintain a foothold long enough to sneak away before the law catches up with him.

#### The Police
In the past year or so, the federal government has restarted funding to many outlying territories, and this city is no exception. A local police force has been formed, many members having originally hailed from New York City. They've managed to take back a lot of mob territory, but the footholds held by many of them still hold firm.

#### Hacker's Underground
After the collapse, after the grid went down, after most access to the outside world was gone, many simply gave up on the tech they had come to rely on so much in the years prior. However, wherever there's tech, there are nerds who just don't know when to throw in the towel.

Hooking up solar panels, generators, and HAM radio modems some locally started to setup a close-nit intranet in the surrounding area. Utilizing the outdated but functioning tech that many had simply thrown out. It kept them connected, kept them moving, and eventually, it even helped them organize.

Through a series of disjointed IRC chats, hypertext documents, and other facilities, a number of small groups started to crop up from the local hacker's scene. Many of these members meet in a handful of local nightclubs, such as the venerable Black Orchid.

#### Freelancers
In a city full of nobodies, someone's gotta have the reputation. Freelancers come in all shapes, sizes, and moral characteristics. Some are filling in the gaps where the cops can't get to yet, others are sweeping the last of the mob's cooked past under the rug, while the rest take what they can get, where they can get it.

### Notable Characters

#### Rufo Medici
The man, the myth, the mob boss. Rufo used to rule the town with an iron fist, but more recently his empire is crumbling beneath him. He still has a lot of influence and money though, which he uses to hire various contractors and freelancers to do his bidding. Those within his circles will know to meet his summons in an abandoned parking garage in The Shops.

#### Scott Rorder
Rufo's right hand man, good at gathering intel and blending in to a crowd. Doesn't usually say much, but is always around.

#### Mallory Morgan
A notable arms-dealer, most well known for her cut-throat attitude. She's made a lot of enemies, and is rumored to live out on the water when not actively making contracts.

#### Veronica Lang
A black market car dealer. Sells stolen and stripped vehicles to whoever needs them. If push came to shove, she could probably out-hire Rufo if a clash happened, but Rufo gives her a lot of business so she doesn't meddle, much.

#### Harlo Jamison
Private investigator who started working for the cops a few years back. He's got a criminal background snuffing out back stabbers for the mob, but spends his time now plotting to take them down.

## Example Skills

::: skill
### Quiet
Sneaking, hiding, blending in.
:::

::: skill
### Loud
Enunciating, convincing, intimidating.
:::

::: skill
### Intrusion
Lockpicking, safe cracking, social engineering.
:::

::: skill
### Parkour
Dodging, weaving, jumping, rolling.
:::

::: skill
### Hacking
Coding, wire tapping, data analysis.
:::

::: skill
### Pyro
Fire, explosives, chemicals.
:::

::: skill
### Auto
Driving, hot-wiring, navigation.
:::

::: skill
### Grease
Hot-rodding, make-shifting,  board-building.
:::

::: skill
### Moxy
Spite, perseverance, luck.
:::

::: skill
### Aim
Guns, knives, baseballs.
:::

## Bonds

### People

::: character-option
#### A Close Friend

* Have a conversation with them
* Learn something new about them
* Share an experience with them
:::

::: character-option
#### A Lover

* Solve a disagreement with them
* Learn something new about them
* Share an intimate moment with them
:::

::: character-option
#### A Rival

* Foil a plan of theirs, or have a plan be foiled by them
* Find a common enemy between you
:::

::: character-option
#### A Mentor

* Go to them for advice
* Be taught something new
* Share a story of your travels with them
:::

### Places

::: character-option
#### A Base of Operations

* Make a plan in your base
* Make an improvement to your base
* Defend your base against intrusion
:::

::: character-option
#### A Place of Business

* Use your line of work in a heist
* Find a way out of work to do..other things
* Actually do your job
:::

### Interests and Backgrounds

::: character-option
#### A Hobby

* Use your hobby as an aid in a heist
* Spend downtime on your hobby
* Talk about your hobby with your co-conspirators
:::

::: character-option
#### Ex-Military

* Draw from your military training
* Utilize a contact from your military days
* Share a story from your time in the military
:::

### Memberships

::: character-option
#### The Mob

* Be brought onto a job
* Use a mafia contact to help with a heist
* Do something you don't like for the mob
:::

::: character-option
#### The Hacker's Underground

* Meet with another member(s) of the underground
* Utilize your connections to gain information
* Share information with your fellow hackers
:::

### Traumas

::: character-option
#### Widowed

* Reminisce with a memento from your love
* Draw on your inspiration of their memory
:::

::: character-option
#### Left for Dead
:::

## Banes

::: character-option
#### Smoker

Whenever you are in a stressful situation, you feel the urge to smoke, chew, or otherwise get your Nicotine fix. If you don't, you gain a level of the strained condition.
:::

::: character-option
#### Alcoholic

This Bane has two parts:
First, if you have not drank any alcohol today you gain a level of the strained condition.

Second, whenever you drink an alcoholic beverage, you need to complete a Difficulty 10 challenge or compulsively drink another.
:::

::: character-option
#### Kleptomaniac

Whenever you see something of value or interest to you that isn't yours, make a Luck Check. On a failure, you must add a d6 to the difficulty of any challenges while within line of sight of the object.
:::

::: character-option
#### Crippled

Whenever you use a resource that requires you to use your crippled limb, you must reroll 6s.
:::

## Boons

::: character-option
#### Steady Source of Income

It's not much, but you have a steady source of income between heists. Whenever there is a time jump between heists, you gain 1 cash point.

:::

::: character-option
#### Hard Worker

You're no stranger to a hard day's work. Once per day, whenever you have less than half of your Stamina, you can take a 15 minute break to gain 1 point of it back.

:::

::: character-option
#### Shortcut Guy

You don't just know your way around the city, you know your way around the city like nobody else does. You always know a shortcut, no matter what it is, you _always_ know a shortcut.
:::

::: character-option
#### Social Engineer

Having been in the industry, you know the look, the jargon, and the types of clients who'd need you. You have the getup to blend in as someone legitimately doing work, and the words to pull it off too. Whenever you pose as someone in a field you are familiar with, you are inconspicuous.

:::

::: character-option
#### Court Jester

You can gain one resource when talking to someone by making up a legal loophole or useless law that backs you up somehow. They will believe you, though they may not like it.

:::

::: character-option
#### Jack of All Trades

A Jack of all trades, master of none, is often better than a master of one. Whenever you use a resource with a Skill you don't have any Levels in, you may add +1 to the roll.
:::

::: character-option
#### Risk Taker

Sometimes it's ok to cut corners, especially if you know what you're doing. Whenever your GM calls for a risk roll, you may roll it with advantage.
:::

::: character-option
#### Director

Whenever your team makes a detailed plan to do something, you gain a resource for every person on your team. You may spend these at any time so long as you are following the plan.
:::

::: character-option
#### Good Luck

Whenever you roll a 1 for a resource, you may reroll that die. You must use the new result though, even if it is another 1.
:::

::: character-option
#### Intuitive

If you are unsure about your situation, or a course of action, you can fish for an intuition, and choose to trust your gut. Make a Luck check, on a success the GM will give you a vague "assured", "uneasy", or similar feeling about your quandary. The answer must truthfully reflect the situation, but does not need to give further insight than that. On a failure, your intuition is neutral and gives no strong indication.
:::

::: character-option
#### The right place, at the right time

You have a knack for showing up just in the nick of time. Once per heist, you may appear at a place you can see in an insignificant amount of time, having already made the move to be there before having used this ability.
:::

::: character-option
#### I know a guy

Once per heist, you can name an NPC that you know and have decent rapport with. They are nearby to your current location and available to meet with you.
:::

::: character-option
#### Oh yeah, I've got one of those

A bit of a pack rat, you've collected a number of useful items that come in handy every now and again. Once per heist, you can name a pocketable item that you have on your person, or a larger item you have stashed somewhere you have access to.
:::

::: character-option
#### Medic

If anyone on your team gains the strained condition, you may spend an hour to patch them up. They'll heal 1 level of the condition, and you cannot heal them again using this until they have had a full night's rest.

:::

::: character-option
#### Tough Guy

Whenever you take a level of the strained condition, you may spend a Stamina to shrug it off and disregard that level of the condition.

:::

::: character-option
#### Overly Dramatic

You can gain one resource by acting in a way that makes your words more convincing, without needing to take a risk for it.
:::

::: character-option
#### Well Known

You can use your reputation to help get you out of sticky situations. If this is the first time you've fully interacted with an NPC, you may roll a Luck Check.

On a success, the person you are talking to recognizes you, and you gain a resource from this interaction.
:::

::: character-option
#### Black Market

Because of your connections, you know people who can obtain certain illicit items and substances for you to purchase. You can do so discretely, and your GM may allow you to call in favors from NPCs you have history with instead of payment.
:::

::: character-option
#### Smuggler

You gain a +1 to all resource rolls for challenges pertaining to smuggling, or hiding items from somebody else.
:::

::: character-option
#### Coupon Clicker

Wth your connections and experience, you have more ready access to guns that others don't. You can purchase guns for half-price.
:::

::: character-option
#### Hard Bargain

Your group gets paid an extra d6 from heists which are commissioned from a client.
:::

::: character-option
#### Car Jacker

You have a shop you can go to with all of the items needed to strip and repaint a stolen vehicle. You can do this in a matter of hours, and the vehicle appears to be a completely different unit to the untrained eye.
:::

## Conditions

::: condition
#### Intoxicated

* **Cause:** An alcoholic beverage.
* **Effect:** You gain -1 from your Dice Pool for every challenge.
* **Stacking:** Yes.
* **Resolution:** You will need to allow the alcohol to filter out of your body. For every hour that you don't drink you will lose a level of this condition.
:::

## Equipment

### Cash Level
As you complete heists and gain reputation, you'll obtain more available funds for yourself. This is represented by your cash level, which starts at 1 and goes up to 5. All Equipment is marked with a cash level for which it is available, represented by a money meter ( **\$$$**$$ ). You can only take equipment which is within or below your current cash level, however you can take as much equipment as you can carry within that.

At the end of each successful heist, your GM will award cash points, which add towards your progress to the next cash level. You'll need 10 Points to increase your cash level. Your GM will ask all of the players the following questions, giving everyone 1 point for each that applies:

* Was someone's life in peril?
* Did you bring/do anything extra?
* Were you undetected?
* Did you secure a new contact?

When you first build your Character, you may select 2 items which are from any cost level, which you will have obtained already.


### Bags

::: item :::
[Backpack]{.name}

[1]{.cost}

You can only wear 1 backpack at a time.

:::: properties
* Container: 12
::::
:::

::: item :::
[Shoulder Bag]{.name}

[1]{.cost}

A bag for your shoulder.

:::: properties
* Container: 5
::::
:::

::: item :::
[Fanny Pack]{.name}

[1]{.cost}

You may retrieve items from this bag without disadvantage on your resource roll.

:::: properties
* Container: 2
::::
:::

### Infiltration Supplies

::: item :::
[Grappling Hook]{.name}

[2]{.cost}

Used to hook onto ledges, most useful attached to some rope.
:::

::: item :::
[Paracord]{.name}

[1]{.cost}

Thinner climbing rope. 30ft of the stuff.
:::

::: item :::
[Crowbar]{.name}

[1]{.cost}

Also known as a prybar.
:::

::: item :::
[Disguise]{.name}

[3]{.cost}

A change of clothes, a little makeup, some hair product. Just enough to pull this off once.

:::: properties
* Consumable
::::
:::

::: item :::
[Lock Picks]{.name}

[2]{.cost}

A set of tools to open things that are locked.
:::

### General Tools

::: item :::
[Flashlight]{.name}

[2]{.cost}

LED flashlight. Much bright, very power.
:::

::: item :::
[Candle]{.name}

[1]{.cost}

Less bright, more messy. Burns for a few hours.

:::: properties
* Consumable
::::
:::

::: item :::
[Hammer]{.name}

[1]{.cost}

When all you have is a hammer, all your problems look like a nail.
:::

::: item :::
[Blanket]{.name}

[1]{.cost}

Useful for those long nights away from home.
:::

::: item :::
[Bell]{.name}

[1]{.cost}

It makes noise.
:::

::: item :::
[Toolbox]{.name}

[2]{.cost}

A general set of hand tools. Heavy and bulky, this must be carried in one hand.
:::

::: item :::
[Walky Talky]{.name}

[3]{.cost}

Comes as a pair, these let you communicate over a short distance.

:::

### Pyrotechnics

::: item :::
[Match Box]{.name}

[1]{.cost}

Strike the match on the side of the box.

:::: properties
* Ammunition: 20
::::
:::

::: item :::
[Blowtorch]{.name}

[3]{.cost}

Like matches, but bigger, hotter, and brighter.
:::

::: item :::
[Breaching Charge]{.name}

[4]{.cost}

A small, self contained explosive which blasts open a door with a directional blast.

:::: properties
* Consumable
::::
:::

::: item :::
[C4]{.name}

[4]{.cost}

General purpose explosive, useful for blowing things up.

:::: properties
* Consumable
::::
:::

::: item :::
[Gunpowder Rocket]{.name}

[3]{.cost}

An unpredictable paper projectile. Usually in the form of a firework, sometimes modified to remove the ending explosion.

:::: properties
* Consumable
::::
:::

### Armor

::: item :::
[Kevlar Vest]{.name}

[5]{.cost}

A piece of ballistics armor.
:::

::: item :::
[Riot Shield]{.name}

[5]{.cost}

A tall bulletproof shield.
:::

### Guns and Ammunition

::: item :::
[Pistol]{.name}

[3]{.cost}

A semi-automatic handgun. Holds 1 magazine.

:::: properties
* Ammunition: 7
::::
:::

::: item :::
[Tommy Gun]{.name}

[5]{.cost}

A fully automatic rifle. Holds 1 drum magazine.

:::: properties
* Ammunition: 100
::::
:::

### Vehicles and Fuel

::: item :::
[Car]{.name}

[4]{.cost}

Standard sedan-style vehicle. Burns a unit of gasoline per heist.
:::

::: item :::
[Truck]{.name}

[5]{.cost}

Bigger, more gas-hungry vehicle. Can carry more stuff.
:::

### Animal Companions

::: item :::
[Dog]{.name}

[2]{.cost}

A good boy. Doesn't take up space in your inventory, but should be kept on a leash.
:::

### Computing

::: item :::
[Laptop]{.name}

[3]{.cost}

Not a new model by any means, but useful for all things technical.
:::

::: item :::
[Hard Drive]{.name}

[2]{.cost}

Bulky but reliable..as long as you don't drop it.
:::

::: item :::
[Thumb Drive]{.name}

[2]{.cost}

Small in storage, but easier to hide.
:::

::: item :::
[Wifi Router]{.name}

[3]{.cost}

Useful for setting up a local network.
:::

::: item :::
[Bluetooth Speaker]{.name}

[2]{.cost}

Wireless sound is a nifty thing, ain't it?"
:::

::: item :::
[Webcam]{.name}

[2]{.cost}

USB Camera that you can hook to a computer.
:::

::: item :::
[Microphone]{.name}

[2]{.cost}

Cheap USB mic, it doesn't sound great, but it'll do.
:::

### Consumables

::: item :::
[Cigarettes]{.name}

[1]{.cost}

Tobacco cancer sticks.

:::: properties
* Ammunition: 12
::::
:::

::: item :::
[Liquor]{.name}

[2]{.cost}

Single container of mass market alcohol.
:::

::: item :::
[Moon Shine]{.name}

[1]{.cost}

Cheap, but grants 2 levels of Intoxicated when drank.
:::

## GM Tools

### Heist Target Organizations

* Rival Mob
* Government/Military
* Independent Collector
* Independent Company

### Vehicle Heists

* Train
* Armored Truck
* Boat
* Semi Truck

### Building Heists

* Bank
* Casino
* Museum
* Headquarters
* Abandoned Facility

### Heist MacGuffins

* Money
* Historical Artifact
* Rare resource
* Piece of tech or equipment
* Person

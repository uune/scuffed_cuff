function Span(block)
    if block.classes[1] == "cost" then
        local cost = block.attributes["cost"] or 1
        local bolded = ("$"):rep(cost)
        local unbolded = ("$"):rep(5 - cost)

        return pandoc.Span(pandoc.List{pandoc.Strong(bolded), pandoc.Str(unbolded)}, {class = "cost"})
    end
end

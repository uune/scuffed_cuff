function cleanup {
    rm build/base.css
    rm build/scuffed_cuff.html
}

trap cleanup EXIT

function build_markdown {
    pandoc --lua-filter=assets/filters.lua --template assets/template -o "$@"
}

rm -r build/*
mkdir -p build/

cp assets/base.css build

build_markdown build/scuffed_cuff.html scuffed_cuff.md
weasyprint build/scuffed_cuff.html build/scuffed_cuff.pdf

cp assets/character_sheet.pdf build/
